FROM node
COPY . /workspace
WORKDIR /workspace
EXPOSE 3000
RUN npm install
ENTRYPOINT [ "node","index.js"]
