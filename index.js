const express = require('express')
const cors = require('cors')
const {Datastore} = require('@google-cloud/datastore')
const {PubSub, Message} = require('@google-cloud/pubsub');
const pubsub = new PubSub({projectId:'town-issue-review-board-p2'});
const {readFile,writeFile} = require('fs').promises;

const datastore = new Datastore();
const app = express();
app.use(express.json());
app.use(cors());

// Helper Functions
async function pullMessages()
{
    const subscription = pubsub.subscription('issue-analysis-subscription');
    // message always has to be the first parameter for the subscription.on('message'......) function
    subscription.on('message', async (issues) =>{
        const suggestion = issues.data.toString(); // issue.data throws bytes. Using toString makes it into a readable string
        const message = JSON.parse(suggestion)// has to be parsed into a JSON, because without it, the format would be sent to datastore in a wierd format
        message.reviewed = 'false';  //all messages start as unreviewed
        message.highlight = 'false';  //all messages start as unhighlighted
        message.id = Math.random() // message.ig create a new field on the message object
        const key = datastore.key(['Message', String(message.id)])
        const response = await datastore.save({key:key,data:message})
        issues.ack() 
        //when code is running, messages pulled manually 
    })
}
pullMessages();

async function getCountByType(type){
    const currTime = Date.now();
    const dayInMs = 86400000;
    const weekInMs = dayInMs * 7;
    const query = datastore.createQuery('Message').filter('issueType','=', type)
    const result = await datastore.runQuery(query)
    const last24Hours = result[0].filter(i => i.timestamp > currTime - dayInMs);
    const last7days = result[0].filter(i => i.timestamp > currTime - weekInMs);
    return [result[0].length, last24Hours.length, last7days.length];
}

async function getCountUnreviewed(){
    const query = datastore.createQuery('Message').filter('reviewed','=', 'false')
    const result = await datastore.runQuery(query)
    return result[0].length;
}

async function getCountReviewed(){
    const query = datastore.createQuery('Message').filter('reviewed','=', 'true')
    const result = await datastore.runQuery(query)
    return result[0].length;
}

// Routes

app.get("/issues", async(req,res)=>{

    if(req.query.type)
    {
        //const Finaltype = String(req.query.type)
        const query = datastore.createQuery('Message').filter('issueType','=',req.query.type)
        console.log("got here")
        const result = await datastore.runQuery(query)
        console.log(result[0])
        res.send(result[0])

    }
    else if(req.query.dateposted)
    {
        const query = datastore.createQuery('Message').filter('datePosting','=',req.query.dateposted)
        const result = await datastore.runQuery(query)
        console.log(result[0])
        res.send(result[0])
    }
    else if(req.query.dateofissue)
    {
        const query = datastore.createQuery('Message').filter('dateIssue','=',req.query.dateofissue)
        const result = await datastore.runQuery(query)
        console.log(result[0])
        res.send(result[0])
    }
    else if(req.query.reviewed)
    {
        const query = datastore.createQuery('Message').filter('reviewed','=',req.query.reviewed)
        const result = await datastore.runQuery(query)
        console.log(result[0])
        res.send(result[0])
    }
    else if(req.query.highlighted)
    {
        const query = datastore.createQuery('Message').filter('highlighted','=',req.query.highlighted)
        const result = await datastore.runQuery(query)
        console.log(result[0])
        res.send(result[0])
    }
    else{
        // get all issues
        const query = datastore.createQuery('Message')
        const result = await datastore.runQuery(query)
        res.send(result[0])
    }
})


// numberOfIssues()
app.get("/issues/report", async(req,res)=>{

    //May not be needed
    //await writeFile('./logs/report.txt','')

    const issueTypes = [
            'infrastructure',
            'safety',
            'public health',
            'pollution',
            'noise/disturbing the peace',
            'other'
        ];

    const issuesObject = {
        last24Hours: {},
        last7Days: {},
        issuesReviewed: 0,
        issuesNotReviewed: 0
    };

    for (const type in issueTypes){
        const counts = await getCountByType(issueTypes[type])
        issuesObject[issueTypes[type]] = counts[0]
        issuesObject.last24Hours[issueTypes[type]] = counts[1];
        issuesObject.last7Days[issueTypes[type]] = counts[2];
    }

    issuesObject.issuesReviewed = await getCountReviewed();
    issuesObject.issuesReviewed = await getCountUnreviewed();

    // Write to file. This is the file that will be saved locally
    // Need to create a PVC yaml file
    
    const currentdate = new Date();

    //doing this just so that 

    function leadZero(val){
        if(val < 10)return "0" + val;
        else return String(val);
    }

    const datetime = leadZero(currentdate.getDate()) + "/"
               + leadZero(currentdate.getMonth() + 1) + "/"
               + currentdate.getFullYear() + " @ "  
               + leadZero(currentdate.getHours()) + ":"  
               + leadZero(currentdate.getMinutes()) + ":" 
               + leadZero(currentdate.getSeconds()) + " ";

    let fileData = null;
    try{
        fileData = await readFile('report/report.txt');
    }catch{
        await writeFile('report/report.txt', '');
        fileData = await readFile('report/report.txt');
    }
    let saveString = fileData.toString();
    saveString = saveString + datetime + JSON.stringify(issuesObject) + '\n';
    await writeFile('report/report.txt',saveString)
    // Send back response
    res.send(JSON.stringify(issuesObject));
})


// Added Routes //

//get report.txt
app.get("/issues/report/file", async(req,res)=>{
    try{
        const file = await readFile('report/report.txt');
        const retStr = file.toString();
        res.send(retStr);
    res.send();
    }catch(error){
        res.status(404);
        res.send('Error, no reports found')
    }
});

//read get a single employee

app.get("/highlight/:issueId", async(req,res)=>{
    const issueId = req.params.issueId;
    console.log(String(issueId))
    const key = datastore.key(['Message', issueId]);
    const [result, metadata] = await datastore.get(key);
    const issue = {}
    issue.key = key;
    issue.data = result;
    if (issue.data.highlight ==='false'){
        issue.data.highlight = 'true';
    }else{
        issue.data.highlight = 'false';
    }
    datastore.update(issue)
    res.send()
})

app.get("/review/:issueId", async(req,res)=>{
    const issueId = req.params.issueId;
    console.log(String(issueId))
    const key = datastore.key(['Message', issueId]);
    const [result, metadata] = await datastore.get(key);
    const issue = {}
    issue.key = key;
    issue.data = result;
    if (issue.data.reviewed ==='false'){
        issue.data.reviewed = 'true';
    }else{
        issue.data.reviewed = 'false';
    }
    datastore.update(issue)
    res.send()
})

app.get("/", (req, res) =>{
    res.send("hi")
})


const PORT = 3000;

app.listen( PORT, () => {
    console.log(`Application started on PORT ${PORT}`);
});


